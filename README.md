# Project Euler Exercise Installer

This is a small script that fetches and sets up Project Euler exercises on your local
machine. Upon installing an exercise, it creates, inside the `projecteuler/` directory, 
first an `exercises/` subdirectory, and then the directory for the requested exercise, 
e.g. `exercise_5` 

## Installation

Clone this repo anywhere. Export the path to the root dir to an environment variable called `EULER_DIR`.

## Usage

Always make sure you are in the root directory of the repo before executing any commands.

### Install

To install an exercise, run the following in a terminal:

```bash
./app/bin/euler exercise install 6
```

where `6` is the number of the exercise.

### Remove

To remove an already installed exercise, run:

```bash
./app/bin/euler exerciseremove 6
```
where `6` is the number of the exercise.
