require_relative '../src/output'

describe Output do
  describe '#println' do

    it 'outputs the string correctly' do
      output_string = 'This string should be outputted to the terminal.'

      expect { Output.println output_string }.to output("#{output_string}\n").to_stdout
    end

    it 'outputs an empty line when no string provided' do
      expect { Output.println }.to output("\n").to_stdout
    end
  end
end