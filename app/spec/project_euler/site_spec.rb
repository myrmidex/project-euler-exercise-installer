require_relative '../../src/initialize'

describe ProjectEuler::Site do

  describe '#fetch_exercise_data' do

    it 'fetches problem statement and answer for an exercise' do
      problem_statement, problem_answer = ProjectEuler::Site.fetch_exercise_data(1)

      expect(problem_statement).to eq 'If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.Find the sum of all the multiples of 3 or 5 below 1000.'
    end
  end
end