require 'fileutils'
require 'nokogiri'
require 'open-uri'

require_relative 'file_handler'
require_relative 'output'
require_relative 'project_euler/help'
require_relative 'project_euler/errors/exercise_already_installed_error'
require_relative 'project_euler/errors/exercise_not_found_error'
require_relative 'project_euler/exercise'
require_relative 'project_euler/site'

# Check if environment_variable EULER_DIR exists
if ENV['EULER_DIR'].nil?
  Output.println 'Missing EULER_DIR environment variable.'
  exit
end

# Create exercises directory if it does not yet exist
EXERCISE_DIR = "#{ENV['EULER_DIR'].chomp('/')}/exercises"
Dir.mkdir(EXERCISE_DIR) unless Dir.exist?(EXERCISE_DIR)