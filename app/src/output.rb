class Output
  class << self
    def println(string = '')
      printf "#{string}\n"
    end
  end
end