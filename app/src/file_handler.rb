class FileHandler
  class << self
    def mkdir(relative_path)
      raise ExerciseAlreadyInstalledError if Dir.exist?(full_path(relative_path))

      FileUtils.mkdir_p(full_path(relative_path))
    end

    def touch(relative_path)
      FileUtils.touch(full_path(relative_path))
    end

    def dir_exists?(relative_path)
      Dir.exist?(full_path(relative_path))
    end

    def rm_r(relative_path)
      FileUtils.rm_r full_path(relative_path)
    end

    def append(relative_path, text)
      open(full_path(relative_path), 'a') { |f| f.puts text }
    end

    private

    def full_path(relative_path)
      "#{ENV['EULER_DIR'].chomp('/')}/#{relative_path}"
    end
  end
end