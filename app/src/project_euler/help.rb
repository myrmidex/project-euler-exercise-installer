module ProjectEuler
  class Help
    def self.show
      Output.println
      Output.println "Project Euler Exercise Installer"
      Output.println
      Output.println "Commands:"
      Output.println "euler help                         Show the help page"
      Output.println "euler exercise install <number>    Install the provided exercise"
      Output.println "euler exercise remove <number>     Remove the provided exercise"
      Output.println
    end
  end
end
