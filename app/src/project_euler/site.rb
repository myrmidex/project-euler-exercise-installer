module ProjectEuler
  class Site
    class << self

      def fetch_exercise(exercise_number)
        e = Exercise.new
        e.number = exercise_number

        problem_statement = fetch_exercise_data(exercise_number)[:problem_statement]

        e.problem_statement = problem_statement

        e
      end

      def fetch_exercise_data(exercise_number)
        html = URI.open("https://projecteuler.net/problem=#{exercise_number}")
        doc = Nokogiri::XML(html,&:noblanks)

        { problem_statement: doc.search('#content .problem_content').text }
      end
    end
  end
end