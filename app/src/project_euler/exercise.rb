module ProjectEuler
  class Exercise
    attr_accessor :number, :problem_statement

    EXERCISES_DIR = 'exercises'

    def create_files
      # create class
      relative_file_path = "#{path}/exercise.rb"
      FileHandler.touch(relative_file_path)
      FileHandler.append(relative_file_path, "class Exercise\n\nend\n")

      # create test
      relative_file_path = "#{path}/exercise_test.rb"
      FileHandler.touch(relative_file_path)
      FileHandler.append(relative_file_path, "require 'minitest/autorun'\nrequire_relative 'exercise'\n\nclass ExerciseTest < Minitest::Test\n\nend\n")
    end

    def create_readme
      readme_file = "#{path}/README.md"
      FileHandler.touch(readme_file)
      FileHandler.append(readme_file, @problem_statement)
    end

    def path
      "#{EXERCISES_DIR}/exercise_#{@number}"
    end

    def self.install(exercise_number)
      if exercise_number.nil?
        Output.println '[ERROR] Exercise number missing'
        exit
      end

      Output.println "Fetching exercise ##{exercise_number} from projecteuler.net..."

      begin
        exercise = ProjectEuler::Site.fetch_exercise exercise_number
      rescue ExerciseNotFoundError
        Output.println "[ERROR] Exercise ##{exercise_number} not found."
        exit
      end

      begin
        FileHandler.mkdir exercise.path
      rescue ExerciseAlreadyInstalledError
        Output.println "[ERROR] Exercise ##{exercise_number} already installed."
        exit
      end

      Output.println "Exercise directory created."
      exercise.create_files
      exercise.create_readme
      Output.println "Exercise files created."
    end

    def self.remove(exercise_number)
      return Output.println '[ERROR] Exercise number missing' if exercise_number.nil?

      path = "exercises/exercise_#{exercise_number}/"

      # unless Dir.exist?(path)
      unless FileHandler.dir_exists?(path)
        Output.println "[FAILED] Exercise is not installed"
        exit
      end

      FileHandler.rm_r path

      Output.println 'Directory successfully removed.'
    end
  end
end